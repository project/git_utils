## INTRODUCTION

A module to do git stuff on Drupal Admin.
the following options are available for now.

git pull
git log
git reset --hard HEAD ; git pull

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/git_utils

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/git_utils


## REQUIREMENTS

No special requirements.


## INSTALLATION

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


## CONFIGURATION

No configuration is needed.


## MAINTAINERS

Current maintainers:
 * Yusef Mohamadi (yuseferi) - https://www.drupal.org/u/yuseferi
