<?php

namespace Drupal\git_utils\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GitUtilsActionsForm.
 */
class GitUtilsActionsForm extends FormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->messenger = $container->get('messenger');
    $instance->loggerFactory = $container->get('logger.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'git_utils_actions_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['git_log'] = [
      '#type' => 'submit',
      '#value' => $this->t('RUN GIT LOG'),
      '#title' => $this->t('RUN GIT LOG'),
      '#submit' => ['::submitForm'],
    ];

    $form['soft_pull'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run Git Pull'),
      '#title' => $this->t('RUN GIT PULL'),
      '#submit' => ['::softPullSubmit'],
    ];

    $form['clean_pull'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run Git Clean and Pull'),
      '#title' => $this->t('RUN GIT PULL'),
      '#submit' => ['::cleanWithPullSubmit'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function softPullSubmit(array &$form, FormStateInterface $form_state) {
    $config = $this->config('git_utils.gitutilsconfig');
    $gitRootDirectory = $config->get('git_root_directory');
    $commandToRun = sprintf('cd %s;git pull 2>&1', $gitRootDirectory);
    $output = shell_exec($commandToRun);
    $output = sprintf("<pre>%s</pre>", $output);
    $output = Markup::create($output);
    $this->messenger->addMessage($output);

  }

  /**
   * {@inheritdoc}
   */
  public function cleanWithPullSubmit(array &$form, FormStateInterface $form_state) {
    $config = $this->config('git_utils.gitutilsconfig');
    $gitRootDirectory = $config->get('git_root_directory');
    $commandToRun = sprintf('cd %s;git reset --hard HEAD;git pull  2>&1', $gitRootDirectory);
    $output = shell_exec($commandToRun);
    $output = sprintf("<pre>%s</pre>", $output);
    $output = Markup::create($output);
    $this->messenger->addMessage($output);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('git_utils.gitutilsconfig');
    $gitRootDirectory = $config->get('git_root_directory');
    $commandToRun = sprintf('cd %s;git log --oneline -n 15 2>&1', $gitRootDirectory);
    $output = shell_exec($commandToRun);
    $output = sprintf("<pre>%s</pre>", $output);
    $output = Markup::create($output);
    $this->messenger->addMessage($output);
  }

}
