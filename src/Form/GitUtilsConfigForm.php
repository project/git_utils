<?php

namespace Drupal\git_utils\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GitUtilsConfigForm.
 */
class GitUtilsConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'git_utils.gitutilsconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'git_utils_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('git_utils.gitutilsconfig');
    $form['git_root_directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Git root directory'),
      '#description' => $this->t('Absolute git root directory'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('git_root_directory') ? $config->get('git_root_directory') : DRUPAL_ROOT,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('git_utils.gitutilsconfig')
      ->set('git_root_directory', $form_state->getValue('git_root_directory'))
      ->save();
  }

}
